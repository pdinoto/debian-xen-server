
## General idea

Create a debian testing Xen server from scratch, to be deployed on a bare metal server (which might be under physical control or not), automating as much as possible its setup.

Explored a manual debian setup, then Ansible playbooks.
- Resulted in too much work documenting the debian setup

Decided to
- Create a iPXE boot configuration file which starts d-i 
- Create a URL-accessible d-i preseed to automate as much as possible
-
A

## Preseed

References:
- Official [preseed file](https://www.debian.org/releases/stretch/example-preseed.txt)
- Initial idea from reading [this gist](https://gist.github.com/devicenull/4348614)
- More info [here](https://github.com/panticz/preseed/blob/master/ipxe/menu.netinstall.ipxe)
  - There are images ready on this github repo, but not much instructions
