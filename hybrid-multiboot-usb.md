
## Idea

Have a multi-ISO boot USB

Already used several versions of this.
Wanting to "have it all":
- Be legacy BIOS and UEFI capable
- Have latest Grub in each case
- Have multiple ISOs to choose from
  - iPXE
  - Linux Mint, live
  - Maybe some Xen boot testing?

Random notes:
- Found that Fedora? installer DVD is already an hybrid ISO?
  (Yes, but the geometry of the disk is set into it, leaving unused and unuseable space on the USB drive)
- Newer instructions on this [Archlinux page](https://wiki.archlinux.org/index.php/Multiboot_USB_drive)
- Data about EFI system partition (missing instrutions about how to format it): [here](https://wiki.archlinux.org/index.php/EFI_System_Partition)
- More data: [Multiboot USB](https://github.com/aguslr/multibootusb), [GLIM: GRUB2 Live ISO Multiboot](https://github.com/thias/glim)
